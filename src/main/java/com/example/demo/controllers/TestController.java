package com.example.demo.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.validation.constraints.Min;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.constants.ControllersConstants;
import com.example.demo.generics.Generics;

/**
 * 
 * @author Zarrabal
 *
 */
@RestController
@RequestMapping(value = ControllersConstants.BASE_URI_API)
@Validated
public class TestController {
	protected static Logger log = LoggerFactory.getLogger(TestController.class);

	@RequestMapping(value = ControllersConstants.BINARY_URI, method = RequestMethod.GET)
	public ResponseEntity<?> getBinary(@Min(1) @RequestParam("input") Integer input) {
		HashMap<String, Object> response = new HashMap<String, Object>();
		Character ch = '1';
		boolean continueIteration =  Boolean.TRUE;
		
		List<Character> chars = Integer.toBinaryString(input)
								       .chars() 
								       .mapToObj(charc -> (char) charc)
								       .collect(Collectors.toList());
		
		Long digitsNumbers = chars.parallelStream()
								  .filter(charc -> charc.equals(ch))
								  .count();
		
		log.info("Input binary representation ==> {}", Integer.toBinaryString(input));
		
		response.put("inputValue", input);
		response.put("inputBinary", Integer.toBinaryString(input));
		
		//smallest
		Integer decSmallestNumber = input;
		
	    while ( continueIteration &&  decSmallestNumber > 0) {
	    	decSmallestNumber--;
	    	
	    	List<Character> charsLargest = Integer.toBinaryString(decSmallestNumber)
											      .chars()
											      .mapToObj(charc -> (char) charc)
											      .collect(Collectors.toList());

			Long digitsLargest = charsLargest.parallelStream()
											 .filter(charc -> charc.equals(ch))
											 .count();
			
			if(digitsNumbers.equals(digitsLargest)) {
				log.info("The next smallest is ==> {}", decSmallestNumber);
				log.info("The next smallest bynary is ==> {}", Integer.toBinaryString(decSmallestNumber));
				continueIteration = Boolean.FALSE;
			}
	    }
	    
	    if(!continueIteration) {
	    	log.info("The next smallest bynary is the same ==> {}", input);
	    }
	    
	    response.put("smallestNumber", decSmallestNumber);
		response.put("smallestBinary", Integer.toBinaryString(decSmallestNumber));
		
	  //largest
		continueIteration =  Boolean.TRUE;
		Integer incLargestNumber = input;
		
	    while ( continueIteration ) {
	    	incLargestNumber++;
	    	
	    	List<Character> charsLargest = Integer.toBinaryString(incLargestNumber)
											      .chars()
											      .mapToObj(charc -> (char) charc)
											      .collect(Collectors.toList());

			Long digitsLargest = charsLargest.parallelStream()
											 .filter(charc -> charc.equals(ch))
											 .count();
			
			if(digitsNumbers.equals(digitsLargest)) {
				log.info("The next largest decimal is ==> {}", incLargestNumber);
				log.info("The next largest bynary is ==> {}", Integer.toBinaryString(incLargestNumber));
				continueIteration = Boolean.FALSE;
			}
	    }
	    
	    response.put("largestNumber", incLargestNumber);
		response.put("largestBinary", Integer.toBinaryString(incLargestNumber));
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = ControllersConstants.ROW_COLUM_CERO_URI, method = RequestMethod.GET)
	public ResponseEntity<?> convertRowColumnToCero() {
		
		Integer [ ] [ ] scores = { { 1, 2, 3 },
					               { 4, 0, 6 },
					               { 7, 8, 9}
					         	 };
		
		Integer row = null;
		Integer colum = null;
		
		outerloop: for(int contRow = 0; contRow < scores.length; contRow++) {
			 row = contRow;
	        
			for(int contColumn = 0; contColumn < scores[contRow].length; contColumn++) {
				colum = contColumn;
				
	        	Integer value = scores[contRow][contColumn];
	        	
	        	if(value.equals(0)) {
	        		break outerloop;
	        	}
	        }
	    }
		
		/**
		 * 
		 */
		for(int contRow = 0; contRow < scores.length; contRow++) {
			for(int contColumn = 0; contColumn < scores[contRow].length; contColumn++) {
	        	if(row.equals(contRow)) {
	        		scores[contRow][contColumn] = 0;
	        	}else if(!row.equals(contRow) && colum.equals(contColumn)) {
	        		scores[contRow][contColumn] = 0;
	        	}
	        }
		}	
		
		log.info("value of the new scores {}", Arrays.deepToString(scores));
		
		HashMap<String, Object> response = new HashMap<String, Object>();
		response.put("result", Arrays.deepToString(scores));
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = ControllersConstants.FIND_LOCATION_ARRAY_URI, method = RequestMethod.GET)
	public ResponseEntity<?> findLocationArray(@RequestParam(value = "input", required = true) String input) {
		
		// Get index of the word in the array
		String[] array = {"at", "", "", "", "ball", "", "", "car", "", "", "dad", "", "" };
		
		Integer index = IntStream.range(0, array.length)
								 .filter(i -> input.equalsIgnoreCase(array[i]))
								 .findFirst()
								 .orElse(-1);
		
		log.info("The value of the index is ==> {}", index);
		
		index = Arrays.stream(array)
					  .collect(Collectors.toList())
					  .indexOf(input);
		
		HashMap<String, Object> response = new HashMap<String, Object>();
		response.put("result", index);
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = ControllersConstants.PERMUTATION_URI, method = RequestMethod.GET)
	public ResponseEntity<?> getpermutation(@Length(min = 1, max = 50) @RequestParam(value = "input", required = true) String input) {
		List<String> response = new ArrayList<>();
		
		Generics.permutations(input, StringUtils.EMPTY, "father", response);
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = ControllersConstants.PARENTHESES_URI, method = RequestMethod.GET)
	public ResponseEntity<?> getParentheses(@Min(1) @RequestParam("input") Integer input) {
		String response = null;

		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
}