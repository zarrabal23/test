package com.example.demo.constants;

public class ControllersConstants {
	public static final String BASE_URI_API = "api/v1/test";
	
	public static final String BINARY_URI = "/binary";
	public static final String ROW_COLUM_CERO_URI = "/rowcolumcero";
	public static final String FIND_LOCATION_ARRAY_URI = "/findlocationarray";
	public static final String PERMUTATION_URI = "/permutations";
	public static final String PARENTHESES_URI = "/parentheses";
}
