package com.example.demo.generics;

import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Generics {
	/**
	 * Method used to generate the permutations of the one string
	 * 
	 * @param input : Attribute that contains the value to process
	 * @param strPermutation : Attribute that contains the permutation storage in each iteration
	 * @param father : Attribute used only to show each of the iterations in which we find ourselves.
	 * @param response
	 */
	public static void permutations(String input, String strPermutation, String father, List<String> response) {
	    
//	    log.info("#######################################################################");
//	    log.info("Value of the input.lenght ==> {}", input.length());
//	    log.info("Value of the strPermutation ==> {}", strPermutation);
//	    log.info("Value of the input ==> {}", input);
	    
	    outerloop: for (int i = 0; i < input.length(); i++) {
//	    	log.info("Value of the i ==> {}", i);
	    	
	    	String realFather = father + "." + i;
//	    	log.info("Value of the father ==> {}", realFather);
	    	
	    	char character = input.charAt(i);
			
			String nextPermutation = input.substring(0, i) + input.substring(i + 1);
			
//			log.info("/////////////////////// FOR /////////////////////////////////////");
//			log.info("Value of the input ==> {}", input);
//			log.info("Value of the strPermutation ==> {}", strPermutation);
//			
//			log.info("Value of the character ==> {}", character);
//			log.info("Value of the strPermutation ==> {}", strPermutation + character);
//			log.info("Value of the netPermutation ==> {}", nextPermutation);
			
			if (nextPermutation.length() == 0) {
				response.add(strPermutation + character);
				break outerloop;
	        } 
			
			permutations(nextPermutation, strPermutation + character, realFather, response);
		}
	}
	
	/**
	 * Method used to generate the permutations of the one string using lambda
	 * 
	 * @param input : Attribute that contains the word to generated permutations
	 * @return Stream<String>
	 */
	public static Stream<String> permutationsLambda(String input) {
	    if (input.isEmpty()) {
	        return Stream.of("");
	    }
	    
	    return IntStream.range(0, input.length())
	    				.parallel()
		                .boxed()
		                .flatMap(i ->
		                	permutationsLambda(input.substring(0, i) + input.substring(i+1))
		                					  .map(t -> input.charAt(i) + t)
		                );
	}
}
