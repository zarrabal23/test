Test Anexinet Project
---------------------------------------------------

**Projects Dependencies**
---------------------------------------------------

**Java 8**
Install Java 8 by downloading it from the following link 
https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html


*** After having cloned the project and dependencies, perform the following in a system symbol ***

```
 1.- In the terminal we position ourselves where the cloned project is located by means of git.
	 example: c: // my_project
 2.- We execute the following command:
	 mvn clean install
 3.- After maven generated the compiled we proceed to run the jar generated in the folder of the project in the path "target/test-0.0.1-SNAPSHOT.jar" and execute the following command
	 java -jar test-0.0.1-SNAPSHOT.jar
```

---------------------------------------------------
**Applicación**
The application runs on port 8045 by default

---------------------------------------------------
To execute the services, get the "Postman Services" of the folder "postman_services" located
in the source code of the project in "src/main/resources/".

After this only you need to import this services in postman and run each one


Get the code - do it
------------------------
Clone the repository:

    $ git clone https://zarrabal23@bitbucket.org/zarrabal23/test.git
    
    